-- Question Number 10 --

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *  Select the course number and name, along with the number of semester hours and *
 *  offering department for every course offered by either the Computer Science    *
 * or Mathematics departments.                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
use university

select course_number, course_name, semester_hours, dept_name 
	from course, department
	where dept_name like 'Mathematics' and offering_dept = department.dept_code
	union
		
select course_number, course_name, semester_hours, dept_name
	from course, department
	where dept_name like 'C%S%' and offering_dept = department.dept_code
	
order by dept_name