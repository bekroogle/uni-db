-- Question Number 7 --

/* This query uses aggregate functions to display the number of instructors who are 
 * teaching--or have taught--each course. */

use university
select c.course_name as [Course Names], count(s.instructor) as [Number of Instructors]
from section s, course c
where s.course = c.course_number
group by c.course_name


/* This query extracts the year from the birthdate of all these dead presidents,
 * and uses the data to find the average age of the students. */

select year(getDate()) - avg(year(date_of_birth)) as [Average Student Age]
from credit_hours ch, student s
where s.last_name = ch.last_name and 
	  s.first_name = ch.first_name
