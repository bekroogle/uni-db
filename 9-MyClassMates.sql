-- Question Number 9 --
use university

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Create Table myClasses                                          *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

 select course, section into myClasses
	from grade_tracker
	where student = 1

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Create Table MyClassMates                                       *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

select (last_name + ', ' + first_name) as Name, student.email, course.course_name as [Course Name]
from grade_tracker left outer join myclasses
		on grade_tracker.course = myclasses.course and grade_tracker.section = myclasses.section,
	student, course
	where myclasses.course is not null and 
		student.student_number = grade_tracker.student and 
		student_number <> 1 and 
		course.course_number = grade_tracker.course
	group by first_name, last_name,  course.course_name, student.email
	order by last_name
