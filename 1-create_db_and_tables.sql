-- Question Number 1 --
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Create DB                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */ 
create database university
go

use university

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * Create Department table                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
 create table department (
	[dept_code] int primary key identity,
	[dept_name] varchar(255),
	[college] varchar(255),
	[office_number] int,

	/* Office Phone Number */
	[office_area_code] varchar(3),
	[office_prefix] varchar(3),
	[office_extension] varchar(4))

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Create Course table                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

create table course (
	[course_number] int primary key identity,
	[course_name] varchar(255) not null,
	[desc] text,
	[semester_hours] int not null,
	[offering_dept] int)


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * Create Section table                                              *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

create table section (
	course int,
	section int not null,
	semester varchar(6),
	[year] int not null,
	instructor varchar(255) null,
	primary key (course, section))

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Create Classifications                                            *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
create table classifications (
	name varchar(255),
	hours int)
 
 go

insert into classifications values ('freshman', '0'), ('sophomore', '30'), ('junior', '60'), ('senior','90')

 go

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * Create Student table                                              *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
create table [student] (
	/* Identitification */
	[student_number] int primary key identity,
	[last_name] varchar(max) not null,
	[middle_name] varchar(max),
	[first_name] varchar(max) not null,
	[email] varchar(255),
	[ssn] varchar(11),

	/* Permanent Address - (required) */
	[perm_street_address] varchar(max),
	[perm_city] varchar(max),
	[perm_state] varchar(max),
	[perm_zip] varchar(5),

	/* Current Address - (optional) */
	[cur_street_address] varchar(max) null,
	[cur_city] varchar(max) null,
	[cur_state] varchar(max) null,
	[cur_zip] varchar(5) null,


	/* Permanent Phone */
	[perm_country_code] varchar(2) null,
	[perm_area_code] varchar(3) not null,
	[perm_prefix] varchar(3) not null,
	[perm_extension] varchar(4) not null,

	/* Current Phone - (optional) */
	[cur_country_code] varchar(2) null,
	[cur_area_code] varchar(3)  null,
	[cur_prefix] varchar(3)  null,
	[cur_extension] varchar(4)  null,

	/* Biological details */
	[date_of_birth] date,
	[gender] char not null,

	/* Educational details */
	[major_dept_number] int not null,
	[minor_dept_number] int null,
	[degree_program] varchar(255) null)


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Create Grade_tracker table                                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
create table grade_tracker (
	student int,
	course int not null,
	section int not null,
	letter_grade char)

create table letter_lookup (
	value int,
	letter char)
go

insert into letter_lookup values 
	('4', 'A'),
	('3', 'B'),
	('2', 'C'),
	('1', 'D'),
	('0', 'F');

