/* Question number 3 */

use university
select s.last_name, s.first_name, sum(c.semester_hours) as credits_completed into credit_hours
from student s, course c, grade_tracker g
where s.student_number = g.student and 
	g.letter_grade not like 'W' and 
	c.course_number = g.course
group by s.first_name, s.last_name
order by last_name, first_name