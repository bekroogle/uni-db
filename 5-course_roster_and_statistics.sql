/* Question number 5. */

use university

select c.course_name as [Course Name], count(gt.student) as  [Number of Students],
	 convert(decimal(10,2), avg(convert(decimal(10,2),ll.value))) as [Average Score],
	 convert(decimal(10,2), min(ll.value)) as [Lowest Grade], convert(decimal(10,2), max(ll.value)) as [Highest Grade]
from course c,  grade_tracker gt, letter_lookup ll
where gt.course = c.course_number and
	ll.letter = gt.letter_grade
group by c.course_name