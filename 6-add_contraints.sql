-- Question Number 6 --

use university

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * Alter Department                                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
alter table department
	add constraint department_uniques unique (dept_name)

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * Alter Course                                                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
alter table course
	add constraint fk_course_offering_dept
	foreign key	(offering_dept) references department(dept_code)


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * Alter Section                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
alter table section
	add constraint fk_section_course
	foreign key (course) references course(course_number)

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * Alter Classifications                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
alter table classifications
	add constraint class_uniques unique (name)

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * Alter Student                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
alter table student
	add constraint student_uniques unique (ssn, email)

alter table student
	add constraint chk_student_email check (email like '%@%.%')

alter table student
	add constraint chk_student_gender check (gender in ('M','F','O')) 

alter table student
	add constraint fk_student_major_dept
	foreign key (major_dept_number) references department(dept_code)

alter table student
	add constraint fk_student_minor_dept
	foreign key (minor_dept_number) references department(dept_code)

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * Alter Grade Tracker                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
alter table grade_tracker
	add constraint fk_gradetrk_student
	foreign key (student) references student(student_number)

alter table grade_tracker
	add constraint fk_gradetrk_course_sec
	foreign key(course, section) references section(course, section)

alter table grade_tracker
	add constraint chk_letter_grade_tracker check (letter_grade in ('A','B','C','D','F','W'));

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * Alter Letter Lookup                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
alter table letter_lookup
	add constraint letter_lookup_check_number 
	check (value >= 0 and value <= 4)

alter table letter_lookup
	add constraint letter_lookup_check_letter 
	check (letter in ('A','B','C','D','F','W'))

