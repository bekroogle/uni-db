/* Question Number 8 */

use university
go
select

	(last_name + ', ' +  first_name) as [Name],				-- Name

	perm_street_address + ' ' +								-- Address
	perm_city + ', ' + perm_state + ' ' + 
	perm_zip as [Permanent Address],
	
	date_of_birth [D.O.B..],								-- DOB						

	degree_program as [Current Program],					-- Degree Program

	d.dept_name as [Major],									-- Major

	(select d.dept_name										-- Minor
		from department d, student s 
		where d.dept_code = s.minor_dept_number and 
		s.last_name = 'Kruger') as [Minor],
		
	(select sum(c.semester_hours)							-- Total Hours
		from grade_tracker gt, course c, student s 
		where c.course_number = gt.course and 
		gt.student = s.student_number and 
		s.last_name = 'Kruger') as [Total Hours],

	(select sum(convert(decimal(10,2),						-- Quality Points
		ll.value * c.semester_hours)) 
		as [Quality Points] 
		from letter_lookup ll, course c, grade_tracker gt, student s
		where ll.letter = gt.letter_grade and 
		gt.course = c.course_number and 
		gt.student = s.student_number and
		s.last_name = 'Kruger') as [Quality Points]
	
	into temp												-- Store in temporary table
	from student, department d
	where last_name = 'Kruger' and 
		d.dept_code = major_dept_number

go															-- Execute Dependencies	

alter table temp
	add [GPA] as [Quality Points] / [Total Hours]			-- Add computed GPA to temp table

go															-- Execute Dependencies	

select c.course_name as [Course],						-- courses i've taken
	(str(c.course_number) + ' ' + str(sec.section)) as Section,
	gt.letter_grade as [Grade],						-- letter grade i've received in the courses

	convert(decimal(10,2),							-- Credit Hours
		c.semester_hours) as [Credit Hours],

	convert(decimal(10,2),							-- Attempted Hours
		 ll.value * c.semester_hours)
		 as [Quality Points],

	sec.year as [Year]								-- Year
	
	into temp_record								-- Store in temporary table
	from course c, grade_tracker gt, 
		student, letter_lookup ll, 
		section sec
	where gt.student = student.student_number and
		student.last_name = 'Kruger' and 
		gt.course = c.course_number and
		ll.letter = gt.letter_grade and 
		sec.course = gt.course and 
		gt.section = sec.section

	order by year

go															-- Execute Dependencies	

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 * Generate Transcript                                                 *	
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
set nocount on
use university
go
select convert(varchar(25),[Name]) as [Name],
	convert(varchar(50),[Permanent Address]) as [Permanent Address],
	[D.O.B..] as [D.O.B.]
	from temp
go
select
	  
	convert(varchar(20),[Current Program]) as [Current Program],
	convert(varchar(20),[Major]) as [Major], 
	convert(varchar(20),[Minor]) as [Minor]
from temp
go
select 
	convert(varchar(20), [Course]) as [Course],
	--convert(varchar(20), [Course]) as [Course],
	[Grade], [Credit Hours], [Quality Points]
from temp_record
go
select [Total Hours], [Quality Points], [GPA] 
from temp
go