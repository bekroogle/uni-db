/* Question number 2 */

use university
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Department                                                      *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
insert into department values 
/*1*/	('Computer Science', 		'Mathematics &	Computer Science',	'329','918','555','1123'),
/*2*/	('Mathematics', 			'Mathematics & Computer Science',	'146','918','555','1120'),
/*3*/	('Political Science', 		'Fine Arts', 						'220','918','555','2000'),
/*4*/	('Computer Engineering',	'Architecture & Engineering', 		'330','918','555','0014'),
/*5*/	('Economics', 				'Business & Technology',			'405','918','555','2275');
go

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Course                                                          *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
insert into course values 
	--01 algorithms
	('Algorithms', 'This course covers the essential information that every serious programmer needs to know about algorithms and data structures, with emphasis on applications and scientific performance analysis of Java implementations. Part I covers basic iterable data types, sorting, and searching algorithms.', '3', '1'),
	
	--02 democratic development
	('Democratic Development', 'Gain an understanding of the political, social, cultural, economic, institutional and international factors that foster and obstruct the development and consolidation of democracy. It is hoped that students in developing or prospective democracies will use the theories, ideas, and lessons in the class to help build or improve democracy in their own countries.', '3', '3'),
	
	--03 fund. of elec. eng.
	('Fundamentals of Electrical Engineering', 'This course probes fundamental ideas in electrical engineering, seeking to understand how electrical signals convey information, how bits can represent smooth signals like music and how modern communication systems work.', '4', '4'),
	
	--04 algebra
	('College Algebra', 'Linear and quadratic equations and inequalities; relations, functions, inverse functions; exponential and logarithmic functions; systems of equations; zeros of polynomials and determinants; permutations, combinations and the binomial theorem, as well as other selected topics. Prerequisite: Placement and enrollment in this course is based on ACT and/or CPT scores, or a combination of ACT and high school grades in mathematics.', '3', '2'),
	
	--05 cs 1
	('Computer Science I', 'The focus is on the problem-solving/software construction process. This will include problem analysis, program design and program coding in modern programming languages. Program style, documentation, algorithms, data structures, procedure and data oriented modularization, component reuse, abstraction, and program verification are introduced early. The primary goal is to motivate and introduce principles and program design in a first course.', '4', '1'),

	--06 machine learning
	('Machine Learning', 'Learn about the most effective machine learning techniques, and gain practice implementing them and getting them to work for yourself.', '3', '1'),

	--07 macro-economics
	('Macroeconomics', 'An introduction to modern macroeconomics. Topics considered include a general overview of basic markets and the U.S. economy, international trade, national income accounts, the nature and causes of the business cycle, unemployment, inflation and growth, along with fiscal and monetary policy.', '3', '4'),

	--08 speech 1
	('Speech', 'An introductory course designed to prepare students to handle the major types of communication situations that they will encounter in life, including interpersonal communication, group communication, and public speaking. No major or minor credit.', '3', '4'),

	--09 calculus 1
	('Calculus I', 'Functions, limits, derivatives, applications of the derivative, the definite integral, the Fundamental Theorem of Calculus and applications of integration.', '4', '2'),

	--10 entrepreneurship
	('Entrepreneurship', 'Topics include history and impact of entrepreneurial activity; characteristics of entrepreneurs; women and minority entrepreneurs; opportunity recognition and evaluation; risk assessment; market identification; and creativity.', '3', '4');
go

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Section                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
insert into section values 
	('1',	'1',	'Summer',	'2011',	'A. Turing'),	/* algorithms       */       
	('1',	'2',	'Spring',	'2011', 'A. Turing'),   /* algorithms	    */          
	('2',	'1',	'Fall',		'2011',	'N. Chomsky'),  /* democratic dev.  */            
	('2',	'2',	'Winter',	'2011',	'B. Franklin'),	/* democratic dev.  */            
	('3',	'1',	'Spring',	'2012', 'E. Fermi'),    /* e.e.             */ 
	('4',	'1',	'Spring',	'2012', 'E. Fermi'),    /* algebra          */    
	('4',	'2',	'Fall',		'2013',	'A. Einstein'), /* algebra          */    
	('4',	'3',	'Summer',	'2013',	'A. Einstein'), /* algebra          */    
	('5',	'1',	'Spring',	'2012', 'E. Fermi'),    /* cs I             */ 
	('6',	'1',	'Fall',		'2011',	'P. Dick'),		/* machine learning */             
	('7',	'1',	'Spring',	'2014',	'A. Einstein'), /* macro-econ       */       
	('8',	'1',	'Fall',		'2011',	'P. Dick'),		/* machine learning */             
	('9',	'1',	'Spring',	'2011', 'A. Turing'),   /* calc I           */   
	('10',	'1',	'Spring',	'2012',	'E. Musk');		/* entrepreneurship */
go

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Student                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
 insert into student values
		 
		 /* 01 */
		 ('Kruger',	'Jay','Benjamin','bekroogle@gmail.com','345-92-3496',
		 '705 S College AVE','Tahlequah','Oklahoma','74464',NULL,NULL,NULL,NULL,
		 '1','335','336','3970',NULL,NULL,NULL,NULL,'9/24/1981','M','1','2','B.S.'),
	


		 /* 02 */
		 ('Washington', NULL, 'George', 'number1prez@us.gov', '378-58-3440',
		 'Mount Vernon','Alexandria','Virginia','22206',NULL,NULL,NULL,NULL,
		 NULL, '367','429','4072','1','418','422','3382','2/22/1732','M','3','5','M.S'),
	

		 
		 /* 03 */
		 ('Jefferson', NULL, 'Thomas', 'mr_declaration@authors.org', '396-51-4207',
		 'Monticello','Charlottesville','Virginia','02213',NULL,NULL,NULL,NULL,
		 NULL, '369','395','3924','1','353','364','4207','03/04/1798','M','3',NULL,'B.S.'),
	

	
		 /* 04 */
		 ('Madison', NULL, 'James', 'dollmad@badnames.org', '383-03-3485',
		 '1 John Adams Blvd','Boston','Massachusetts','02802',NULL,NULL,NULL,NULL,
		 NULL, '344','333','3816','1','405','399','3840','8/8/1840','M','1','2','B.A.'),
	

		 
		 /* 05 */
		 ('Monroe', NULL, 'James', 'jamon@prez.com', '424-43-3731',
		 '1 John Adams Blvd','Boston','Massachusetts','02203',NULL,NULL,NULL,NULL,
		 NULL, '353','336','4260','1','393','427','4283','2/2/1293','M','2','3','Ph.D.'),
	

		 
	 	 /* 06 */
		 ('Adams', 'Quincy', 'John', 'papaAdams@us.gov', '375-60-3695',
		 '1 John Adams Blvd','Boston','Massachusetts','03402',NULL,NULL,NULL,NULL,
		 NULL, '343','340','3763','1','408','357','3617','4/4/1408','M','5','2','B.S.'),
	

		 
		 /* 07 */
		 ('Jackson', NULL, 'Andrew', 'IndianHitler@trailoftears.org', '386-20-3787',
		 '1 John Adams Blvd','Boston','Massachusetts','02908',NULL,NULL,NULL,NULL,
		 NULL, '395','351','4085','1','334','348','4062','9/9/1934','M','4',NULL,'B.S.'),
	

		
		 /* 08 */
		 ('Van Buren', NULL, 'Buren', 'vanRuin@us.gov', '366-81-3712',
		 '1 John Adams Blvd','Boston','Massachusetts','08702',NULL,NULL,NULL,NULL,
		 NULL, '378','360','3933','1','335','358','3928','7/7/1735','M','4','1','B.S.'),
	

		
		 /* 09*/
		 ('Harrison', 'Henry', 'William', '2old2serv@us.gov', '409-47-3925',
		 '1 John Adams Blvd','Boston','Massachusetts','02706',NULL,NULL,NULL,NULL,
		 NULL, '377','362','4004','1','351','334','3666','7/7/1751','M','2','5','B.S.'),
	

		
		 /* 10 */
		 ('Tyler', NULL, 'John', 'neverelected@us.gov', '381-98-4150',
		 '1 John Adams Blvd','Boston','Massachusetts','06201',NULL,NULL,NULL,NULL,
		NULL, '415','370','4288','1','399','424','3888','08/30/1399','M','1','5','B.A.');

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Enrollment Records (Grade Tracker)                              *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

insert into grade_tracker values
	('1',	'10',	'1',	'B'),
	('1',	'2',	'1',	'A'),
	('1',	'4',	'1',	'B'),	
	('1',	'1',	'2',	'A'),
	('1',	'3',	'1',	'A'),
	('1',	'5',	'1',	'A'),
	('2',	'4',	'2',	'B'),
	('2',	'8',	'1',	'D'),
	('3',	'7',	'1',	'F'),
	('3',	'8',	'1',	'A'),
	('3',	'9',	'1',	'A'),
	('4',	'5',	'1',	'C'),
	('4',	'8',	'1',	'A'),
	('4',	'9',	'1',	'C'),
	('5',	'2',	'2',	'W'),
	('5',	'3',	'1',	'A'),
	('5',	'6',	'1',	'C'),
	('5',	'9',	'1',	'A'),
	('6',	'3',	'1',	'W'),
	('6',	'8',	'1',	'C'),
	('7',	'1',	'1',	'C'),
	('7',	'10',	'1',	'C'),
	('7',	'2',	'1',	'D'),
	('7',	'3',	'1',	'F'),
	('7',	'7',	'1',	'D'),
	('8',	'7',	'1',	'D'),
	('9',	'1',	'2',	'W'),
	('9',	'4',	'3',	'C'),
	('9',	'6',	'1',	'A'),
	('10',	'10',	'1',	'D'),
	('10',	'5',	'1',	'C'),
	('10',	'6',	'1',	'A'),
	('10',	'8',	'1',	'W'),
	('10',	'2',	'2',	'D'),
	('10',	'7',	'1',	'A')
